//no.1
console.log('No.1 Arrow function')

const golden = () => {
    console.log("this is golden!!")
    console.log('\n')
}
golden()

//no.2
console.log('No.2 Object literal')

const newFunction = (firstName, lastName) => {
    firstName
    lastName
    return {
            fullName() {
            return console.log(firstName + ' ' + lastName + '\n') 
        }
    }
}

newFunction("William", "Imoh").fullName()

//no.3
console.log('No.3 Destructuring')

const newObject = {
    firstName : 'Harry',
    lastName : 'Potter Holt',
    destination : 'Hogwarts React Conf',
    occupation : 'Deve-wizard Avocado',
    spell : 'Vimulus Renderus!!!'
}

const {firstName, lastName, destination, occupation, spell} = newObject

console.log(firstName, lastName, destination, occupation, spell)
console.log('\n')

//no.4
console.log('No.4 Array Spreading')

const west = ['Will', 'Chris', 'Sam', 'Holly']
const east = ['Gill', 'Brian', 'Noel', 'Maggie']
const combined = [...west, ...east]

console.log(combined)
console.log('\n')

//no.5
console.log('No.5 Template Literals')

const planet = 'earth'
const view = 'glass'
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)